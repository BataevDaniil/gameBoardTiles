// @flow

export default (countColor: number, countMatche: number) => [
	'191', 'f22', '400', '819',
	'383', '400', 'f22', '772',
	'055', '6f6', '772', '191',
	'6f6', '055', '819', '383',
].map(color => `#${color}`);
