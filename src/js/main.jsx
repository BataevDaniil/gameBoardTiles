// @flow

import React from 'react';
import { render } from 'react-dom';

import Game from './Game';

import generatorColors from './generatorColors';

const domElem = document.querySelector('#game');
if (domElem)
	render(<Game
		colCount={4}
		rowCount={4}
		colorsMatcheCount={2}
		generatorColors={generatorColors}
	/>, domElem);
else
	console.log('html element not exists');
