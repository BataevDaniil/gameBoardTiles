import styled from 'styled-components';

export default styled.div`
	width: 100px;
	height: 100px;
	background: ${props => props.color};
	margin: 10px;
	border-radius: 10px;
`;
