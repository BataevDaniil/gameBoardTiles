import React from 'react';
import { configure, shallow, render, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

import Game from '../index'
import Tile from '../styledComponent/Tile'

const generatorColors = (countColor: number, countMatche: number) => [
	'191', 'f22', '400', '819',
	'383', '400', 'f22', '772',
	'055', '6f6', '772', '191',
	'6f6', '055', '819', '383',
].map(color => `#${color}`);

jest.useFakeTimers();

describe('Game board 4x4 and matche count tiles = 2.', () => {
	let wrapper;
	let colors = generatorColors(16, 2);
	beforeEach(() => {
		wrapper = shallow(
			<Game
				colCount={4}
				rowCount={4}
				colorsMatcheCount={2}
				generatorColors={generatorColors}
			/>
		)
	});

	test('Component did mount view all tiles open then sometime hide all tiles', () => {
		let Tiles = wrapper.find(Tile);
		expect(Tiles.length).toBe(colors.length);

		// view all tiles open
		for (let i = 0; i < Tiles.length; i++)
			expect(Tiles.at(i).props().color).toBe(colors[i]);

		jest.runAllTimers();
		wrapper.update();

		Tiles = wrapper.find(Tile);
		expect(Tiles.length).toBe(colors.length);

		// hide all tiles
		for (let i = 0; i < Tiles.length; i++)
			expect(Tiles.at(i).props().color).toBe('#000');
	});

	test('Click 2 same tiles then 2 this tiles is none (change color on none tile) and other tiles is hide.', () => {
		jest.runAllTimers();
		wrapper.update();
		let Tiles = wrapper.find(Tile);

		//Click 2 same tiles
		let oneTile = 0, twoTile = 11;
		Tiles.at(oneTile).simulate('click');
		Tiles.at(twoTile).simulate('click');

		wrapper.update();
		Tiles = wrapper.find(Tile);

		// 2 this tiles is none
		expect(Tiles.at(oneTile).props().color).toBe('#fff');
		expect(Tiles.at(twoTile).props().color).toBe('#fff');

		// other tiles is hide
		for (let i = 0; i < Tiles.length; i++)
			if (i !== oneTile && i !== twoTile)
				expect(Tiles.at(i).props().color).toBe('#000');
	});

	test('Click 2 different tiles then view all tiles open then sometime hide all tiles', () => {
		jest.runAllTimers();
		wrapper.update();
		let Tiles = wrapper.find(Tile);

		// Click 2 different tiles
		let oneTile = 0, twoTile = 1;
		Tiles.at(oneTile).simulate('click');
		Tiles.at(twoTile).simulate('click');

		Tiles = wrapper.find(Tile);
		expect(Tiles.length).toBe(colors.length);

		// view all tiles open
		for (let i = 0; i < colors.length; i++)
			expect(Tiles.at(i).props().color).toBe(colors[i]);

		jest.runAllTimers();
		wrapper.update();

		Tiles = wrapper.find(Tile);
		expect(Tiles.length).toBe(colors.length);

		// hide all tiles
		for (let i = 0; i < Tiles.length; i++)
			expect(Tiles.at(i).props().color).toBe('#000');
	});

	for (let i = 0; i < colors.length; i++)
		test(`Click a tile then a tile open and other tiles is hide: number ${i}, color ${colors[i]}`, () => {
			jest.runAllTimers();
			wrapper.update();
			let Tiles = wrapper.find(Tile);

			// Click a tile
			Tiles.at(i).simulate('click');

			// a tile open
			wrapper.update();
			Tiles = wrapper.find(Tile);
			expect(Tiles.at(i).props().color).toBe(colors[i]);

			// other tiles is hide
			for (let j = 0; j < Tiles.length; j++)
				if (j !== i)
					expect(Tiles.at(j).props().color).toBe('#000');
		});

	test('Click 2 same tiles then 2 this tiles is none (change color on none tile) and other tiles is hide, this again to the end then view all tiles open then sometime hide all tiles.', () => {
		jest.runAllTimers();
		wrapper.update();

		// '191', 'f22', '400', '819',
		// '383', '400', 'f22', '772',
		// '055', '6f6', '772', '191',
		// '6f6', '055', '819', '383',
		const yetHide = [];
		[
			{oneTile: 0, twoTile: 11}, // 191
			{oneTile: 1, twoTile: 6}, // f22
			{oneTile: 2, twoTile: 5}, // 400
			{oneTile: 3, twoTile: 14}, // 819
			{oneTile: 4, twoTile: 15}, // 383
			{oneTile: 7, twoTile: 10}, // 772
			{oneTile: 8, twoTile: 13}, // 055
			// last {oneTile: 9, twoTile: 12}, // 6f6
		].map(({ oneTile, twoTile }) => {
			wrapper.update();
			let Tiles = wrapper.find(Tile);

			//Click 2 same tiles
			Tiles.at(oneTile).simulate('click');
			Tiles.at(twoTile).simulate('click');

			wrapper.update();
			Tiles = wrapper.find(Tile);

			// 2 this tiles is none
			expect(Tiles.at(oneTile).props().color).toBe('#fff');
			expect(Tiles.at(twoTile).props().color).toBe('#fff');

			yetHide.push(oneTile, twoTile);
			// other tiles is hide
			for (let i = 0; i < Tiles.length; i++)
				if (yetHide.indexOf(i) === -1)
					expect(Tiles.at(i).props().color).toBe('#000');
		})
		wrapper.update();
		let Tiles = wrapper.find(Tile);

		//Click 2 same tiles
		let oneTile = 9, twoTile = 12;
		Tiles.at(oneTile).simulate('click');
		Tiles.at(twoTile).simulate('click');

		wrapper.update();
		Tiles = wrapper.find(Tile);
		// view all tiles open
		for (let i = 0; i < colors.length; i++)
			expect(Tiles.at(i).props().color).toBe(colors[i]);

		jest.runAllTimers();
		wrapper.update();

		Tiles = wrapper.find(Tile);
		expect(Tiles.length).toBe(colors.length);

		// hide all tiles
		for (let i = 0; i < Tiles.length; i++)
			expect(Tiles.at(i).props().color).toBe('#000');
	});
})
