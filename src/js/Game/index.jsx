// @flow

import React from 'react';

import Tile from './styledComponent/Tile';

import { type GameProps, type GameState } from '../flowTyped';

class Game extends React.Component<GameProps, GameState> {
	colCount: number;
	rowCount: number;
	colorsMatcheCount: number;
	colors: string[];

	noneColor = '#fff';
	hideColor = '#000';
	timeViewBoard = 2000;

	constructor(props: GameProps) {
		super(props);
		this.colCount = this.props.colCount;
		this.rowCount = this.props.rowCount;
		this.colorsMatcheCount = this.props.colorsMatcheCount;
		this.colors = this.props.generatorColors(this.colCount * this.rowCount, this.colorsMatcheCount);

		this.state = {
			board: this.colors,
			chooseTile: this.resetChooseTile(),
			countNoneTile: 0,
		};
		this.timeoutViewDefaultBoard();
	}

	render() {
		// console.log('colors = ', this.colors);
		// console.log('game state = ', this.state);
		// console.log('game props = ', this.props);
		return (
			<div className='board'>
				{
					this.state.board.map((color, index) => (
						<Tile
							key={color + index.toString()}
							color={color}
							onClick={this.handlerTileClick(this.colors[index])(index)}
						/>
					))

				}
			</div>
		);
	}

	handlerTileClick = (color: string) => (index: number) => () => {
		const { chooseTile, board, countNoneTile } = this.state;

		if (board[index] === this.noneColor) return;
		if (board[index] === color && board[index] !== this.hideColor) return;

		if (chooseTile.color !== '' && chooseTile.color !== color || countNoneTile + chooseTile.count + 1 === board.length) {
			this.resetGame();
			return;
		}

		if (chooseTile.count + 1 === this.colorsMatcheCount) {
			this.setState({
				board: board.map((defaultColor, i) => (
					this.colors[i] === chooseTile.color ? this.noneColor : defaultColor
				)),
				chooseTile: this.resetChooseTile(),
				countNoneTile: countNoneTile + this.colorsMatcheCount,
			});
			return;
		}

		this.setState({
			board: board.map((defaultColor, i) => (i === index ? color : defaultColor)),
			chooseTile: { color, count: chooseTile.count + 1 },
		});
	}

	resetGame = () => {
		this.setState({
			board: this.colors,
			chooseTile: this.resetChooseTile(),
			countNoneTile: 0,
		});
		this.timeoutViewDefaultBoard();
	}

	resetChooseTile = () => ({
		color: '',
		count: 0,
	})

	resetBoard = () => {
		const arr = [];
		for (let i = 0; i < this.colCount * this.rowCount; i++)
			arr.push(this.hideColor);
		return arr;
	}

	timeoutViewDefaultBoard = () => {
		setTimeout(() => {
			this.setState({
				board: this.resetBoard(),
			});
		}, this.timeViewBoard);
	}
}

export default Game;
