// @flow

export type GameProps = {
	+colCount: number,
	+rowCount: number,
	+colorsMatcheCount: number,
	generatorColors: (number, number) => Array<string>,
};

export type GameState = {
	board: string[],
	chooseTile: {
		color: string,
		count: number,
	},
	countNoneTile: number,
}
