# Description

The page consists of a board of tiles (default 4x4).
- every 2 tiles have the same color (it could be an image, it doesn't matter);
- each tile is closed, so user can not see a color (image).

The gameplay is the sequence of rounds. In each round user should select 2
tiles with the same color to make them disappear. If he selects 2 tiles with
different colors then they are flipped to "closed" state, and user proceeds
with the next round. The game is considered to be over when all of the tiles
are opened.

# Build

```bash
yarn install
```
if development
```bash
yarn development
```

# Test

```bash
yarn test
```
if with mode watch files then
```bash
yarn test:watch
```
if view coverage then
```bash
yarn test:coverage
```

# Possible errors

[one error](https://stackoverflow.com/questions/16748737/grunt-watch-error-waiting-fatal-error-watch-enospc)

Solution
```
echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p
```
[two error](https://github.com/sindresorhus/gulp-autoprefixer/issues/83)

Solution: update nodejs and remove folder node_modules and again make ```npm install```
